# [POC] Script for code testing automation

This is made as a POC for a [reddit question](https://www.reddit.com/r/AskProgramming/comments/if4x8p/is_there_a_software_to_automatize_tests/).

The proporsal goes around AssertJ and Single File Execution.

CheckClasses.java is the script where you could provide the Subject Under Test (SUT) and all required checks.

This is a simple POC, please check AssertJ for a better understandment [this](https://www.baeldung.com/introduction-to-assertj) is a good starting point (but notice that most tutorials will assume you're developing, not grading/testing).

## Execution
![img](img/execution.png)

To execute a single file in a script style you need at least Java 11.

Notice that you will need to add the assertj JAR and the classes (with full package structure) to the classpath (check the structure of testCases for that last part).
