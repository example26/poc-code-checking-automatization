import com.example.ClassToTest;

import static org.assertj.core.api.Assertions.*;


public class CheckClasses {

    public static void main(String[] args){

        System.out.println("Start testing...");

        ClassToTest subjectUnderTest = new ClassToTest();


        System.out.println("Testing for 5");
        assertThat(subjectUnderTest.addFive(5)).isEqualTo(10);


        System.out.println("Testing for -5");
        assertThat(subjectUnderTest.addFive(-5)).isEqualTo(0);

    }

}
